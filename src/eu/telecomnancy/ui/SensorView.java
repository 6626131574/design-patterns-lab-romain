package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.command.OffCommand;
import eu.telecomnancy.sensor.command.OnCommand;
import eu.telecomnancy.sensor.command.UpdateCommand;

public class SensorView extends JPanel implements Observer {

	private static final long serialVersionUID = 1617547979853706560L;

	private AbstractObservableSensor sensor;
	private OffCommand offCommand;
	private UpdateCommand updateCommand;
	private OnCommand onCommand;

	private JLabel value = new JLabel("N/A °F");
	private JButton on = new JButton("On");
	private JButton off = new JButton("Off");
	private JButton update = new JButton("Acquire");

	public SensorView(AbstractObservableSensor sensor2) {
		this.sensor = sensor2;
		onCommand = new OnCommand(sensor2);
		offCommand = new OffCommand(sensor2);
		updateCommand = new UpdateCommand(sensor2);

		sensor2.addObserver(this);
		this.setLayout(new BorderLayout());

		value.setHorizontalAlignment(SwingConstants.CENTER);
		Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
		value.setFont(sensorValueFont);
		this.add(value, BorderLayout.CENTER);

		on.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onCommand.Execute();
			}
		});

		off.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				offCommand.Execute();
			}
		});

		update.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateCommand.Execute();
			}
		});

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new GridLayout(1, 4));
		buttonsPanel.add(update);
		buttonsPanel.add(on);
		buttonsPanel.add(off);

		this.add(buttonsPanel, BorderLayout.SOUTH);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		try {
			value.setText(sensor.getValue() + " °F");
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}
}

package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensorAdaptater;
import eu.telecomnancy.ui.ConsoleUI;

public class LegacyTemperatureSensorClient {

	public static void main(String[] args) {
		ISensor sensor = new LegacyTemperatureSensorAdaptater();
		new ConsoleUI(sensor);
	}

}

package eu.telecomnancy;

import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensorAdaptater;
import eu.telecomnancy.sensor.LogSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.decorator.FahrenheitDecorator;
import eu.telecomnancy.sensor.decorator.FloorDecorator;
import eu.telecomnancy.sensor.newstatesensor.NewStateSensor;

public class SensorFactory {
	public static AbstractObservableSensor create(String propertyFile)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		AbstractObservableSensor capteur = null;
		ReadPropertyFile rp = new ReadPropertyFile();
		Properties p;

		try {
			p = rp.readFile(propertyFile);
			if (p.getProperty("sensor").equals("eu.telecomnancy.sensor.TemperatureSensor")) {
				capteur = new TemperatureSensor();
			} else if (p.getProperty("sensor").equals("eu.telecomnancy.sensor.LegacyTemperatureSensorAdaptater")) {
				capteur = new LegacyTemperatureSensorAdaptater();
			} else if (p.getProperty("sensor").equals("eu.telecomnancy.sensor.newstatesensor.NewStateSensor")) {
				capteur = new NewStateSensor();
			}

			if (p.getProperty("FahrenheitDecorator").equals("true")) {
				FahrenheitDecorator fahrenheitDecorator = new FahrenheitDecorator(capteur);
				capteur = fahrenheitDecorator;
			}
			if (p.getProperty("FloorDecorator").equals("true")) {
				FloorDecorator floorDecorator = new FloorDecorator(capteur);
				capteur = floorDecorator;
			}
			if (p.getProperty("Log").equals("true")) {
				LogSensor log = new LogSensor(capteur);
				capteur = log;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return capteur;
	}
}

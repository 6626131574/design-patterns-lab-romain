package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class UpdateCommand extends AbstractCommand {

	public UpdateCommand(AbstractObservableSensor sensor) {
		super(sensor);
	}

	@Override
	public void Execute() {
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}

}

package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.AbstractObservableSensor;

public abstract class AbstractCommand implements ICommand {
	protected AbstractObservableSensor sensor;

	public AbstractCommand(AbstractObservableSensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public abstract void Execute();

}

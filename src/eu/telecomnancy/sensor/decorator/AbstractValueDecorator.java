package eu.telecomnancy.sensor.decorator;

import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class AbstractValueDecorator extends AbstractObservableSensor {
	protected AbstractObservableSensor decoratedSensor;

	public AbstractValueDecorator(AbstractObservableSensor decoratedSensor) {
		this.decoratedSensor = decoratedSensor;
	}

	@Override
	public void on() {
		decoratedSensor.on();
	}

	@Override
	public void off() {
		decoratedSensor.off();
	}

	@Override
	public boolean getStatus() {
		return decoratedSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		decoratedSensor.update();
	}

	@Override
	public abstract double getValue() throws SensorNotActivatedException;
}

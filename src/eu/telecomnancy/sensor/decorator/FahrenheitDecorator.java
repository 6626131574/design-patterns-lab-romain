package eu.telecomnancy.sensor.decorator;

import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class FahrenheitDecorator extends AbstractValueDecorator {

	public FahrenheitDecorator(AbstractObservableSensor decoratedSensor) {
		super(decoratedSensor);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return decoratedSensor.getValue() * 1.8 + 32;
	}
}

package eu.telecomnancy.sensor.newstatesensor;

import java.util.Random;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class OnSensor implements IStateSensor {
	double value = 0;

	@Override
	public boolean getStatus() {
		return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}

}

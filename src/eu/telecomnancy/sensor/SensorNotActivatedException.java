package eu.telecomnancy.sensor;

public class SensorNotActivatedException extends Exception {

	private static final long serialVersionUID = -4220263071581634611L;

	public SensorNotActivatedException(String message) {
		super(message);
	}
}
